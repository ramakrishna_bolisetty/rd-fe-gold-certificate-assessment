import { ComponentFixture, TestBed, inject, waitForAsync } from '@angular/core/testing';
import { RouterTestingModule } from "@angular/router/testing";
import { EditBookComponent } from './edit-book.component';
import { HttpClientTestingModule } from '@angular/common/http/testing';
import { ReactiveFormsModule } from '@angular/forms';
import { BookService } from '../book.service';
import { of } from 'rxjs';
import { Book } from '../book';

describe('EditBookComponent', () => {
  let component: EditBookComponent;
  let fixture: ComponentFixture<EditBookComponent>;
  let element:HTMLElement;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ EditBookComponent ]
      ,imports:[
        HttpClientTestingModule,ReactiveFormsModule,RouterTestingModule
      ]
    })
    .compileComponents();

    fixture = TestBed.createComponent(EditBookComponent);
    component = fixture.componentInstance;
    element=fixture.debugElement.nativeElement;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });


  it('should have the specified labels', () => {
    expect(element.querySelector('.author')).toBeTruthy();
    expect(element.querySelector('.price')).toBeTruthy();
    expect(element.querySelector('.title')).toBeTruthy();
    expect(element.querySelector('.author')?.textContent).toEqual('Author');
    expect(element.querySelector('.price')?.textContent).toEqual('Price');
    expect(element.querySelector('.title')?.textContent).toEqual('Title');

  });



});

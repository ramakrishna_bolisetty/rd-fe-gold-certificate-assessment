import { Component } from '@angular/core';
import { FormControl, FormGroup } from '@angular/forms';
import { ActivatedRoute, ParamMap } from '@angular/router';
import { BookService } from '../book.service';
@Component({
  selector: 'app-edit-book',
  templateUrl: './edit-book.component.html',
  styleUrls: ['./edit-book.component.css']
})
export class EditBookComponent {


  id!: string;

  constructor(private activatedRoute: ActivatedRoute, private bookService: BookService) { }

  ngOnInit() {

    this.activatedRoute.paramMap.subscribe((params: ParamMap) => {
      this.id = params.get("id") || "0";
      this.selectedBook(this.id);
    })

  }

  editBookForm = new FormGroup({
    author: new FormControl(),
    price: new FormControl(),
    title: new FormControl()



  })
  bookD: any;
  selectedBook(id: string) {
    this.bookService.getBookDetails(id).subscribe((bookData) => {
      this.bookD = bookData;
      console.log(bookData);

      this.editBookForm.setValue({
        author: bookData.author,
        price: bookData.price,
        title: bookData.title

      })
    })


  }

  editBook() {
    const editedBookData = this.editBookForm.value;
    this.bookService.updateBookDetails(this.id, editedBookData).subscribe((updatedValue) => {

      console.log('updated value is ', updatedValue);

    })

  }


}

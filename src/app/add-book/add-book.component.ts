import { Component } from '@angular/core';
import { Book } from '../book';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import { BookService } from '../book.service';

@Component({
  selector: 'app-add-book',
  templateUrl: './add-book.component.html',
  styleUrls: ['./add-book.component.css']
})
export class AddBookComponent {

  constructor(private bookService: BookService) { }

  newBook = new FormGroup({
    author: new FormControl('', Validators.required),
    price: new FormControl('', Validators.required),
    title: new FormControl('', Validators.required)

  })

  validateData(data: any) {

    if (data.id == '' || data.author == '' || data.price == '' || data.title == '')
      return 0;
    else
      return 1;
  }

  createBook() {
    let newBookData = {
      id: Date.now() + ((Math.random() * 100000).toFixed()),
      ...this.newBook.value
    }

    const valid = this.validateData(newBookData);
    if (valid == 0) {
      alert('Enter all fields');
      return;
    }
    console.log(newBookData);
    this.bookService.addNewBook(newBookData).subscribe((BookData) => {
      console.log('new Book data is ', BookData);

    });


  }
}

import { ComponentFixture, TestBed, inject, waitForAsync } from '@angular/core/testing';
import { ReactiveFormsModule } from '@angular/forms';
import { AddBookComponent } from './add-book.component';
import { HttpClientTestingModule } from '@angular/common/http/testing';
import { BookService } from '../book.service';
import { of } from 'rxjs';

describe('AddBookComponent', () => {
  let component: AddBookComponent;
  let fixture: ComponentFixture<AddBookComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [AddBookComponent],
      imports: [
        HttpClientTestingModule, ReactiveFormsModule
      ]
    })
      .compileComponents();

    fixture = TestBed.createComponent(AddBookComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it("should call validate when create book is called", waitForAsync(

    inject([BookService], (bookService: BookService) => {
      let book = {
        "id": '1', 'author': 'sd', 'title': 'sample', 'price': '100'
      }
      spyOn(bookService, 'addNewBook').and.returnValue(of(book));
      spyOn(component, 'validateData');
      component.createBook();
      expect(component.validateData).toHaveBeenCalled();

    })

  


  ));



});

import { ComponentFixture, TestBed, inject, waitForAsync } from '@angular/core/testing';
import { HttpClientTestingModule, HttpTestingController } from '@angular/common/http/testing';
import { ViewAllBooksComponent } from './view-all-books.component';
import { RouterTestingModule } from "@angular/router/testing";
import { Book } from '../book';
import { BookService } from '../book.service';
import { of } from 'rxjs';

describe('ViewAllBooksComponent', () => {
  let component: ViewAllBooksComponent;
  let fixture: ComponentFixture<ViewAllBooksComponent>;
  let httpTestController:HttpTestingController;
  let service:BookService;
  let element:HTMLElement;
  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ ViewAllBooksComponent ],
      imports:[HttpClientTestingModule, RouterTestingModule]
    })
    .compileComponents();

    fixture = TestBed.createComponent(ViewAllBooksComponent);
    component = fixture.componentInstance;
    element=fixture.debugElement.nativeElement;
    service = TestBed.inject(BookService);
    fixture.detectChanges();
    httpTestController = TestBed.inject(HttpTestingController);
  });

  let books:Book[];
  beforeEach( ()=>{

     books=[
      {
        "id": "1",
        "title": "title1",
        "author": "rabindranth tagore",
        "price": "200"
      },
      {
        "id": "2",
        "title": "title2",
        "author": "kalam",
        "price": "100"
      },
    ]

    component.books=books;


  })

  it('should create', () => {
    expect(component).toBeTruthy();
  });

 it(' should contain mock books',()=>{
  component.books=books;
  fixture.detectChanges();
  expect(component.books).toBe(books);
 });

 it("should get the users on ngOnInit", waitForAsync(

  inject([BookService], (bookService: BookService) => {
    spyOn(bookService, 'getBooks').and.returnValue(of(books));
    component.ngOnInit();

    expect(bookService.getBooks).toHaveBeenCalled();

  })
  

 ));
 it("should call getBooks when  deleteBook is called", waitForAsync(

  inject([BookService], (bookService: BookService) => {
    spyOn(bookService, 'getBooks').and.returnValue(of(books));
    spyOn(bookService,'deleteBook').and.returnValue(of(books[1]));
    component.deleteBook(books[0]);
    expect(bookService.getBooks).toHaveBeenCalled();

  })
  

 ));


it('shoud contain delete button and edit button',()=>{
  fixture.detectChanges();

  const delEle=element.querySelector('.del-btn');
  const editEle=element.querySelector('.edit-btn');
  expect(delEle).toBeTruthy();
  expect(editEle).toBeTruthy();

})

it('delete and edit methods should be defined',()=>{

 expect(component.deleteBook).toBeDefined();
 expect(component.editBook).toBeDefined();

});


});
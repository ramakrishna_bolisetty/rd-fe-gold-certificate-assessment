import { Component } from '@angular/core';
import { BookService } from '../book.service';
import { ActivatedRoute, Router } from '@angular/router';
import { Book } from '../book';

@Component({
  selector: 'app-view-all-books',
  templateUrl: './view-all-books.component.html',
  styleUrls: ['./view-all-books.component.css']
})
export class ViewAllBooksComponent {




  constructor(private bookService: BookService, private router: Router, private activatedRoute: ActivatedRoute) { }

 
  books!: any;
  ngOnInit() {
    this.getBooks();
  }

  getBooks() {

    this.bookService.getBooks().subscribe((books) => {
      this.books = books;
    })
  }

  searchByTitle(val: any) {
    this.books = this.books.filter((book: any) => book.title.includes(val));

  }

  deleteBook(book: Book) {
    alert("Delete permenantely");

    this.bookService.deleteBook(book.id).subscribe((deletedBook) => {
      console.log('Deleted book suceefully');
      this.getBooks();

    })

  }

  editBook(book: Book) {
    this.router.navigate(['edit', book.id]);
  }
}

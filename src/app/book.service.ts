import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Book } from './book';

@Injectable({
  providedIn: 'root'
})
export class BookService {

  constructor(private http: HttpClient) { }


  url = 'http://localhost:3000/books'

  getBooks() {

    return this.http.get<Book[]>('http://localhost:3000/books');

  }

  getBookDetails(id: string) {

    return this.http.get<Book>(this.url + "/" + id);



  }

  addNewBook(book: any) {

    return this.http.post<Book>(this.url, { ...book });
  }

  updateBookDetails(id: string, bookData: any) {

    return this.http.patch<Book>(this.url + '/' + id, { ...bookData })

  }

  deleteBook(id: string) {

    return this.http.delete<Book>(this.url + '/' + id);
  }



}
